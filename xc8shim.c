// lol fuck u microchip
//
#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#define MCHP_XCLM_VALID_PRO_LICENSE 2

static pid_t (*_waitpid)(pid_t pid, int * wstatus, int options);

pid_t waitpid(pid_t pid, int * wstatus, int options)
{
	static unsigned count;

	if (!_waitpid)
		_waitpid = dlsym(RTLD_NEXT, "waitpid");

	pid_t rc = _waitpid(pid, wstatus, options);

	// Make sure we're not in a fork, don't want to go making a mess of
	// other things' return values
	FILE * comm = fopen("/proc/self/comm", "r");
	char name[256];
	size_t n_read = fread(name, 1, sizeof(name) - 1, comm);
	name[n_read] = 0;
	fclose(comm);

	char * newline = strchr(name, '\n');
	if (newline)
		*newline = 0;

	if (count++ < 3 && !strcmp(name, "cc1"))
		// <<8 is inverse of WEXITSTATUS()
		*wstatus = (MCHP_XCLM_VALID_PRO_LICENSE << 8);

	return rc;
}
